import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { InputComponent } from './input/input.component';
import { OffersComponent } from './offers/offers.component';
import { metaReducers, reducers } from './store';
import { OrdersComponent } from './orders/orders.component';
import { OrderEffects } from './store/order.effects';

@NgModule({
  declarations: [
    AppComponent,
    InputComponent,
    OffersComponent,
    OrdersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers, {metaReducers}),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
    EffectsModule.forRoot([OrderEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
