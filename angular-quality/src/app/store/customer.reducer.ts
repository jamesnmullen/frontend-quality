import { CustomerActions, CustomerActionTypes } from './customer.actions';


export interface Customer {
  name: string;
  number: number | '';
}

export const initialState: Customer = {
  name: '',
  number: ''
};

export function reducer(state = initialState, action: CustomerActions): Customer {
  switch (action.type) {

    case CustomerActionTypes.SetCustomer: {
      return action.customer;
    }

    default:
      return state;
  }
}

export const isPremium = (state: Customer) => !!state.number && state.number > 10000;
